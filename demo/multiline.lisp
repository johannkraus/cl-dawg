(ql:quickload :cl-dawg)

;; multiline input has #\newline in it and has to end with #\newline
(defparameter *multiline* (cl-dawg:make-dawg (concatenate 'string "GTA" (string #\newline) "GTAAAC" (string #\newline) "GT" (string #\newline))))

(= 2 (cl-dawg:freq "TA" *multiline*))
(cl-dawg:freq-kmer 2 *multiline*)
