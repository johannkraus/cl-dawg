(ql:quickload :cl-dawg)

;; Sequence from paper Clift et al., NAR, 14, 141--158, 1986
(defparameter *clift* (cl-dawg:make-dawg "GTAGTAAAC"))

(= 4 (cl-dawg:freq "A" *clift*))
(= 2 (cl-dawg:freq "AA" *clift*))
(= 2 (cl-dawg:freq "TA" *clift*))
(= 2 (cl-dawg:freq "GTA" *clift*))

