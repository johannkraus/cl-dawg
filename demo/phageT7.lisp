(ql:quickload :cl-dawg)

(defun read-fasta (filename)
  (reduce #'(lambda (a b) (concatenate 'string a b))
	  (cdr (with-open-file (stream filename)
		 (loop for line = (read-line stream nil)
		    while line
		    collect line)))))

;; Phage T7 genome, example from paper Clift et al., NAR, 14, 141--158, 1986
(defparameter *phageT7* (cl-dawg:make-dawg (read-fasta "PhageT7.fasta")))

(= 10842 (cl-dawg:freq "A" *phageT7*))
(= 41 (cl-dawg:freq "TCCCT" *phageT7*))

;; check run time
(let ((seq (read-fasta "PhageT7.fasta")))
  (dotimes (i 10)
    (time (cl-dawg:make-dawg seq))))

;; it's about 0.1 sec on my MacBook Pro 2.66 GHz
;; it was about 30 sec on a Pyramid 90X 8 MHz according to Clift et al.
;; might be acceptable speed (~ 1.1)
