(ql:quickload :cl-dawg)

;; Sequence from paper Blumer et al., Theoretical Computer Science, 40, 31--55, 1985
(setf (gethash #\B cl-dawg:*alphabet*) 8)
(setf (gethash #\D cl-dawg:*alphabet*) 9)

(defparameter *blumer* (cl-dawg:make-dawg "ABCBCD")) ;; it's abcbcd in Figure 4

(= 2 (cl-dawg:freq "B" *blumer*))
(= 2 (cl-dawg:freq "C" *blumer*))
(= 2 (cl-dawg:freq "BC" *blumer*))
(= 8 (cl-dawg:find-seq "BCD" *blumer*))
       
