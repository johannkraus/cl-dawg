(ql:quickload :cl-dawg)

;; load phageT7 as example
(defun read-fasta (filename)
  (reduce #'(lambda (a b) (concatenate 'string a b))
	  (cdr (with-open-file (stream filename)
		 (loop for line = (read-line stream nil)
		    while line
		    collect line)))))

;; Phage T7 genome, example from paper Clift et al., NAR, 14, 141--158, 1986
(defparameter phageT7 (cl-dawg:make-dawg (read-fasta "PhageT7.fasta")))

;; check kmer
(cl-dawg:freq-kmer 2 phageT7)
(mapcan #'(lambda (x) (if (> (cdr x) 10) (list x))) (cl-dawg:freq-kmer 10 phageT7))
(some #'(lambda (x) (> x 10)) (map 'list #'cdr (cl-dawg:freq-kmer 15 phageT7)))

