;;;; cl-dawg.asd
;;;; -*- Mode: Lisp; Syntax: COMMON-LISP; Base: 10 -*-

(asdf:defsystem :cl-dawg
  :description "cl-dawg provides a data structure called directed acyclic word graph."
  :long-description "A directed acyclic word graph (DAWG) is a data structure that stores a string (or a set of strings)
                     to efficiently allow for queries whether a given substring belongs to it in time proportional to
                     the substring's length. The DAWG represents suffixes of a given string where edges are labeled with
                     the corresponding characters. Therefore, the paths along the edges of the DAWG from the root node
                     to the terminal node give all possible substrings. The current implementation follows the pseudo-
                     code presented in [1,2]. For a comparison of (compact) DAWG and suffix trees see e.g. [3,4,5]. For
                     a simple example write (cl-dawg:freq 'AAT' (cl-dawg:make-dawg 'AATAATAGAAT')) which should be 3.

                     [1] A. Blumer, J. Blumer, D. Haussler, R.M. McConell, A. Ehrenfeucht. Building a complete inverted 
                         files for a set of text files in linear time. Proc. 16th ACM Symp. Theo. Comp. pages 349-358, 1984.
                     [2] A. Blumer, J. Blumer, D. Haussler, A. Ehrenfeucht, M.T. Chen, J. Seiferas. The smallest automaton
                         recognizing the subwords of a text. Theoretical Computer Science, 40:31-55, 1985.
                     [3] A. Blumer, J. Blumer, D. Haussler, R.M. McConell, A. Ehrenfeucht. Complete inverted files for 
                         efficient text retrieval and analysis. Journal of the ACM, 34:578-589, 1987.
                     [4] A. Blumer, A. Ehrenfeucht, D. Haussler. Average sizes of suffix trees and dawgs. Discrete Applied 
                         Mathematics, 24:37-45, 1989.
                     [5] M. Mohri, P. Moreno, E. Weinstein. General suffix automaton construction algorithm and space bounds.
                         Theoretical Computer Science, 410:3553-3562, 2009.
                     [6] S. Inenaga, H. Hoshino, A. Shinohara, M. Takeda, S. Arikawa, G. Mauri, G. Pavesi. On-line
                         construction of compact directed acyclic word graphs. Discrete Applied Mathematics,
                         146(2):156-179, 2005."
  :version "0.47"
  :author "Johann M. Kraus"
  :licence "MIT"
  :serial t
  :components ((:file "package")
	       (:file "specials")
	       (:file "dawg")
	       (:file "api")))
