;;;; package.lisp

(defpackage :cl-dawg
  (:use :cl)
  (:export :*alphabet*
	   :make-dawg
	   :find-seq
	   :freq
	   :freq-kmer))

