;;;; dawg.lisp

(in-package :cl-dawg)

;; dawg is hash-table of max required size (1- (* 2 (length string)))
;; node is vector of [suffixstate frequency primaryencoding edges]
;; for edges see *alphabet*, i.e. first char (end-char) starts at 3 (this is index in nodevector)
;; primaryencoding is integer telling which edges are primary. Query by logitp and set as sum of 2^index.

(defun init (string)
  "Initialize DAWG with max size"
  (let ((dawg (make-hash-table :size (1+ (* 2 (length string)))))) ; 2n-1, i.e. 2n+1 incl. newline at end
    ;; create source node at hash 0
    (setf (gethash 0 dawg) (make-array (+ (hash-table-count *alphabet*) 3) :element-type '(integer 0 *) :initial-element 0))
    dawg))

(defun split (parentstate childstate dawg)
  "Split childstate and adjust all affected edges and suffixstates."
  (let ((newchildstate (hash-table-count dawg))
	(edge (position childstate (gethash parentstate dawg) :from-end t)))
    ;; create newchildstate: copy suffixstate and all edges from childstate to secondary edges in newchildstate
    (setf (gethash newchildstate dawg) (copy-seq (gethash childstate dawg)))
    (setf (aref (gethash newchildstate dawg) 2) 0) ; reset it's primary info
    ;; reset suffixstate of childstate to newchildstate
    (setf (aref (gethash childstate dawg) 0) newchildstate)
    ;; set primary edge 'a' from parentstate to newchildstate
    (setf (aref (gethash parentstate dawg) edge) newchildstate)
    (setf (aref (gethash parentstate dawg) 2) (+ (expt 2 edge) (aref (gethash parentstate dawg) 2)))
    ;; update edges from above to point to newchildstate
    (do ((currentstate parentstate)
	 (suffixstate (aref (gethash parentstate dawg) 0)))
	((or (= currentstate 0) ; source
	     (null (position childstate (gethash suffixstate dawg) :start 3)) ; no edge
	     (logbitp (position childstate (gethash suffixstate dawg) :start 3) (aref (gethash suffixstate dawg) 2)))) ; primary edge
      (setf currentstate suffixstate)
      (setf suffixstate (aref (gethash currentstate dawg) 0))
      (setf (aref (gethash currentstate dawg) (position childstate (gethash currentstate dawg) :from-end t)) newchildstate))
    newchildstate))

(defun update (currentsink a dawg)
  "Update the dawg given currentsink and base a."
  (let ((newsink (hash-table-count dawg))
	(edge (gethash a *alphabet*)))
    ;; create newsink and set primary edge 'a' from currentsink to newsink
    (setf (gethash newsink dawg) (make-array (+ (hash-table-count *alphabet*) 3) :element-type '(integer 0 *) :initial-element 0))
    (setf (aref (gethash currentsink dawg) edge) newsink)
    (setf (aref (gethash currentsink dawg) 2) (+ (expt 2 edge) (aref (gethash currentsink dawg) 2)))
    ;; set suffixstate for newsink
    (setf (aref (gethash newsink dawg) 0)
	  (do ((currentstate currentsink)
	       (suffixstate 0))
	      ((or (= currentstate 0) (not (= suffixstate 0)))
	       suffixstate)
	    (setf currentstate (aref (gethash currentstate dawg) 0))
	    (if (= (aref (gethash currentstate dawg) edge) 0)
		(setf (aref (gethash currentstate dawg) edge) newsink)
		(if (logbitp edge (aref (gethash currentstate dawg) 2)) ; if primary
		    (setf suffixstate (aref (gethash currentstate dawg) edge))
		    (setf suffixstate (split currentstate (aref (gethash currentstate dawg) edge) dawg))))))
    newsink))

(defun set-freq (dawg)
  "Set frequencies of nodes in dawg starting from source."
  (do ((mystack (make-array (hash-table-size dawg) :element-type '(integer 0 *) :initial-element 0 :fill-pointer 1))
       (fp 1 (fill-pointer mystack))) ; first state is 0
      ((= fp 0)) ; finally back again and out
    (let* ((curr (aref mystack (1- fp)))
	   (currentstate (gethash curr dawg))
	   (freq (do ((i 3 (1+ i)) ; sum of freqs of all edges (if edge not= 0)
		      (sum 0))
		     ((= i (length currentstate))
		      (if (= sum 0) 1 sum)) ; sink if sum over all edges = 0, i.e. return 1
		   (let* ((nextstate (aref currentstate i))
			  (nextfreq (aref (gethash nextstate dawg) 1)))
		     (if (and (not (= nextstate 0)) (= nextfreq 0)); if not yet visited (and if not= edge 0)
			 (progn (vector-push nextstate mystack) ; move forward
				(return -1)) ; maybe next time
			 (setf sum (+ sum nextfreq))))))) ; sum freqs (if = edge 0 nextfreq is 0 anyway)
      (when (> freq 0)
	  (setf (aref currentstate 1) freq) ; yeah set it and go back
	  (vector-pop mystack)))))

;; don't use as recursion will end in stack overflow
(defun set-freq-recursive (currentsink dawg)
  (if (= (reduce #'+ (subseq (gethash currentsink dawg) 3)) 0)
      1
      (setf (aref (gethash currentsink dawg) 1) 
	    (reduce (lambda (sum edge)
		      (setf sum (+ sum (if (<= 1 (aref (gethash edge dawg) 1))
					   (aref (gethash edge dawg) 1)
					   (set-freq-recursive edge dawg)))))
		    (remove 0 (subseq (gethash currentsink dawg) 3)) :initial-value 0))))
