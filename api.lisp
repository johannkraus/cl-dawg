;;;; api.lisp

(in-package :cl-dawg)

(defun make-dawg (string)
  "Build a dawg from a given string."
  (let* ((dawg (init string)))
    (update (reduce #'(lambda (currentsink a) (update currentsink a dawg)) string :initial-value 0) *endchar* dawg)
    (set-freq dawg)
    dawg))

(defun find-seq (seq dawg)
  "Returns the final key for seq or nil if seq not found."
  (reduce #'(lambda (currentstate a)
	      (when (and (not (null currentstate)) (gethash a *alphabet*))
		  (let ((nextstate (aref (gethash currentstate dawg) (gethash a *alphabet*))))
		    (and (> nextstate 0) nextstate)))) seq :initial-value 0))
    
(defun freq (string dawg)
  "Returns the number of times string occurs in dawg."
  (let ((laststate (gethash (find-seq string dawg) dawg)))
    (if laststate
	(aref laststate 1)
	0)))

;; TODO
(defun locations (seq)
  "Returns the set of positions at which seq occurs."
  seq)

(defun freq-kmer (k dawg)
  "Returns all kmers of length k and the number of times they occur in dawg"
  (let ((alphabet (make-array (+ 3 (hash-table-count *alphabet*)) :element-type 'string :initial-element "-")))
    (maphash #'(lambda (key value) (setf (aref alphabet value) (string key))) *alphabet*)
    (do* ((myqueue (let ((q (list nil))) (cons q q))) ; queue with pointer, see norvig-queues
	  (curr (cons "" 0) (car (setf (car myqueue) (cdar myqueue)))) ; dequeue a substring
	  (currentstate (gethash 0 dawg) (gethash (cdr curr) dawg)))
	 ((= (length (car curr)) k)
	  (car myqueue)) ; finally all kmer found
      (do ((i 4 (1+ i)))
	  ((= i (length currentstate)))
	(when (not (= 0 (aref currentstate i)))
	    (let ((new (cons (concatenate 'string (car curr) (aref alphabet i)) (aref currentstate i))))
	      (if (= (length (car new)) k)
		  (progn (setf (cdr new) (aref (gethash (aref currentstate i) dawg) 1))
			 (setf (cdr myqueue) (setf (cddr myqueue) (list new)))) ; at end enqueue kmer with freq
		  (setf (cdr myqueue) (setf (cddr myqueue) (list new)))))))))) ; enqueue next substring
