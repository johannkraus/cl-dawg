;;;; specials.lisp

(in-package :cl-dawg)

(defvar *endchar* #\newline)
(defvar *alphabet* (make-hash-table))
(setf (gethash *endchar* *alphabet*) 3)
(setf (gethash #\A *alphabet*) 4)
(setf (gethash #\C *alphabet*) 5)
(setf (gethash #\G *alphabet*) 6)
(setf (gethash #\T *alphabet*) 7)

